## MonoGame Terminal
A small, simple terminal library for MonoGame 3.7 and .NET Framework 4.5.

---

You can find the [nuget package here](https://www.nuget.org/packages/MonogameTerminal/). Or you can use the package manager console in Visual Studio.

```
Install-Package MonogameTerminal -Version 1.1.0
```

##### Setup
![img](https://i.imgur.com/NU6dKG3.png)  
You will need an [CP437](https://www.wikiwand.com/en/Code_page_437) character sheet. This one above has 16x16 tiles, but any sizes are compatible with this library.

When importing the texture through the MonoGame Content Pipeline, you should set the `ColorKeyColor` to the background color of your image. Also set `ColorKeyEnabled` to `true`. All other settings can be left at default.

![](https://i.imgur.com/1bko0vr.png)

##### Usage

This library is intended to be used with MonoGame. Have a look at the two sample projects to see a basic usage example.

First you need to initialize the terminal, either in the constructor or the `Initialize()` method.

```csharp
_terminal = new MGTerminal(80, 48, new CharSet("terminal16x16", 16, 16), _graphics);
```

You must specify a `CharSet` and pass in the `GraphicsDeviceManager` into the MGTerminal constructor. The name of the `CharSet` must match the name of the font texture file (excluding the file extension).

**Note:** *The MGTerminal constructor will automatically set the window size based on the width and height provided, along with the CharSet character width and character height. Don't try and resize the window after creating the terminal instance.*

Then you need to call the load method, usually from inside the `LoadContent()` method. Here you need to pass in the MonoGame `ContentManager` instance.

```csharp
_terminal.Load(Content);
```

Once everything is setup, you can draw the game to the terminal.

```csharp
private void DrawGame()
{
    _terminal.Clear();

    // Fill the background with '#' characters
    for (int y = 0; y < _terminal.HeightInChars; y++)
    {
        for (int x = 0; x < _terminal.WidthInChars; x++)
        {
            _terminal.Write('#', x, y);
        }
    }

    // Write text to the terminal.
    _terminal.Write("Hello World!", 20, 20, Color.Red);

}
```
The `Clear()` method simply clears the terminal before the next characters are set on the terminal. See what happens when you leave it out!

If you want, you can manually set the foreground and background colors of the characters with the following:

```csharp
terminal.SetForegroundColor(Color.Red, 10, 10);
terminal.SetBackgroundColor(Color.Yellow, 20, 22);
```

The default foreground and background colors are white and black respectively. You can change these with this.
```csharp
terminal.DefaultFgColor = Color.Red;
terminal.DefaultBgColor = Color.Blue;
``` 

You have several options of drawing characters to the terminal. With all the following methods, the foreground and background color parameters are optional. Meaning the character will drawn with the default colors.

```csharp
// Draw a single character with a red foreground color.
Write('@', 12, 34, Color.Red);

// Write text with the default white and black colors.
Write("Hi", 23, 2);
// Write text with a blue background color and the default foreground color.
Write("Hi", 23, 3, bgColor: Color.Blue);

// Write text centered on the x coord, with some lovely colors.
WriteCentered("I'm in the middle", 20, 30, Color.Yellow, Color.Cyan);
```

Finally, you draw the terminal to the screen:
```csharp
_spriteBatch.Begin();
_terminal.Draw(_spriteBatch);
_spriteBatch.End();
```
Note, `spriteBatch.Begin()` and `spriteBatch.End()` must be called around the `terminal.Draw()` method. 


#### Dependencies
+ .NET Framework 4.5
+ MonoGame 3.7

#### Released

+ 1.1.1
    + **Current release**
    + Fixed certain sized textures not loading correctly.
+ 1.1.0
    + Removed string color overriding. I think it was beyond the scope of this library, though I may decide to add it back in a future release.
+ 1.0.0 
    + The initial release, currently unlisted on nuget.
    + This version has the ability to override the color of induvidual words when writing strings.