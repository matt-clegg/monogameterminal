﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameTerminal.Core;
using System;

namespace MonoGameTerminal.Win32
{
    public class BasicSample : Game
    {
        private readonly Random _random = new Random();

        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private MGTerminal _terminal;

        private int _frame;
        private Color _flashColor;

        public BasicSample()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // Create a new MGTerminal, specifying the charset to be used.
            _terminal = new MGTerminal(80, 48, new CharSet("terminal16x16", 16, 16), _graphics);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load assets needed by the terminal.
            _terminal.Load(Content);
        }

        protected override void UnloadContent()
        {
            _spriteBatch.Dispose();

            // Dispose and disposable assets used by the terminal.
            _terminal.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        private void DrawGame()
        {
            _terminal.Clear();

            // Fill the background with '#' characters
            for (int y = 0; y < _terminal.HeightInChars; y++)
            {
                for (int x = 0; x < _terminal.WidthInChars; x++)
                {
                    _terminal.Write('#', x, y);
                }
            }

            // Write text to the terminal.
            _terminal.Write("Hello World!", 20, 20, Color.Red);

            if (_frame++ >= 10)
            {
                _flashColor = RandomColor();
                _frame = 0;
            }

            _terminal.WriteCentered("Flashing Text!", 25, 25, _flashColor, Color.Black);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Draw your game here.
            DrawGame();

            _spriteBatch.Begin();

            // Spritebatch begin and end are needed to call draw.
            // Draw the terminal to the screen.
            _terminal.Draw(_spriteBatch);

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Create a random color with no transparency.
        /// </summary>
        /// <returns></returns>
        private Color RandomColor()
        {
            return Color.FromNonPremultiplied(_random.Next(256), _random.Next(256), _random.Next(256), 256);
        }
    }
}
