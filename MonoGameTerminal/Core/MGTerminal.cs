﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameTerminal.Core
{
    /// <summary>
    /// A simple terminal class to be used with Monogame.
    /// </summary>
    public class MGTerminal
    {
        // 1x1 texture used to render a solid background behind glyphs.
        private readonly Texture2D _bgTexture;

        /// <summary>
        /// The width of the terminal in characters
        /// </summary>
        public int WidthInChars { get; }

        /// <summary>
        /// The height of the terminal in characters
        /// </summary>
        public int HeightInChars { get; }

        /// <summary>
        /// Gets or sets the default foreground color used when drawing glyphs. The default color is white.
        /// </summary>
        public Color DefaultFgColor { get; set; }

        /// <summary>
        /// Gets or sets the default background color used when drawing glyphs. The default color is black.
        /// </summary>
        public Color DefaultBgColor { get; set; }

        private readonly CharSet _charSet;
        private readonly CharTile[] _charTiles;

        // <summary>
        /// Creates a new MGTerminal instance. This constructor will immediately resize the Monogame window based on the
        /// terminal width and height and the character set's width and height.
        /// </summary>
        /// <param name="widthInChars">The terminal width in characters</param>
        /// <param name="heightInChars">The terminal height in characters</param>
        /// <param name="charSet">A specific character set to use.</param>
        /// <param name="graphicsDeviceManager">The Monogame graphics manager.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the width and height are not greater than zero.</exception>
        /// <exception cref="ArgumentNullException">Thrown if the <see cref="CharSet"/> or <see cref="GraphicsDeviceManager"/> is null</exception>
        public MGTerminal(int widthInChars, int heightInChars, CharSet charSet, GraphicsDeviceManager graphicsDeviceManager)
        {
            if (widthInChars <= 0) throw new ArgumentOutOfRangeException(nameof(widthInChars), "Width must be greater than zero.");
            if (heightInChars <= 0) throw new ArgumentOutOfRangeException(nameof(heightInChars), "Height must be greater than zero.");
            if (graphicsDeviceManager == null) throw new ArgumentNullException(nameof(graphicsDeviceManager));

            _charSet = charSet ?? throw new ArgumentNullException(nameof(charSet));

            WidthInChars = widthInChars;
            HeightInChars = heightInChars;

            DefaultFgColor = Color.White;
            DefaultBgColor = Color.Black;

            _charTiles = new CharTile[WidthInChars * HeightInChars];

            _bgTexture = new Texture2D(graphicsDeviceManager.GraphicsDevice, 1, 1);
            _bgTexture.SetData(new[] { Color.White });

            graphicsDeviceManager.PreferredBackBufferWidth = WidthInPixels;
            graphicsDeviceManager.PreferredBackBufferHeight = HeightInPixels;
            graphicsDeviceManager.ApplyChanges();

            InitializeTerminal();
        }

        /// <summary>
        /// The actual width of the game window in pixels.
        /// </summary>
        public int WidthInPixels => WidthInChars * _charSet.CharWidth;

        /// <summary>
        /// The actual width of the game window in pixels.
        /// </summary>
        public int HeightInPixels => HeightInChars * _charSet.CharHeight;

        public char GlyphAt(int x, int y) => CharTileAt(x, y)?.Glyph ?? (char)0;
        public Color? FgColorAt(int x, int y) => CharTileAt(x, y)?.FgColor;
        public Color? BgColorAt(int x, int y) => CharTileAt(x, y)?.BgColor;

        /// <summary>
        /// Whether or not an x and y position is within the terminal bounds.
        /// </summary>
        public bool InBounds(int x, int y) => x >= 0 && y >= 0 && x < WidthInChars && y < HeightInChars;

        private CharTile CharTileAt(int x, int y) => InBounds(x, y) ? _charTiles[GetIndex(x, y)] : null;
        private int GetIndex(int x, int y) => x + y * WidthInChars;

        private void InitializeTerminal()
        {
            for (int i = 0; i < _charTiles.Length; i++)
            {
                _charTiles[i] = new CharTile((char)0, DefaultFgColor, DefaultBgColor);
            }
        }

        /// <summary>
        /// Loads any content needed by the terminal. This must be called before the Draw method is called.
        /// </summary>
        public void Load(ContentManager content)
        {
            _charSet.Load(content);
        }

        /// <summary>
        /// Dispose any disposable content used by the terminal.
        /// </summary>
        public void Dispose()
        {
            _charSet.Dispose();
        }

        /// <summary>
        /// Draws the terminal to a <see cref="SpriteBatch"/>. This method requires Begin() and End() to be called
        /// before and after the method is ran.
        /// </summary>
        /// <param name="batch"></param>
        public void Draw(SpriteBatch batch)
        {
            if (_charSet.Texture == null)
            {
                throw new InvalidOperationException("Cannot draw terminal, load method has not been called.");
            }

            for (int y = 0; y < HeightInChars; y++)
            {
                int drawY = y * _charSet.CharHeight;
                for (int x = 0; x < WidthInChars; x++)
                {
                    int drawX = x * _charSet.CharWidth;

                    CharTile tile = _charTiles[GetIndex(x, y)];

                    char glyph = tile.Glyph;
                    Color fgColor = tile.FgColor;
                    Color bgColor = tile.BgColor;

                    var destination = new Rectangle(drawX, drawY, _charSet.CharWidth, _charSet.CharHeight);

                    batch.Draw(_bgTexture, destination, bgColor);

                    // Only render a glyph if the symbol is not empty.
                    // 0 == ' '
                    if (glyph != 0)
                    {
                        batch.Draw(_charSet.Texture, destination, _charSet.GetBoundsForGlyph(glyph), fgColor);
                    }
                }
            }
        }

        /// <summary>
        /// Clears the terminal. Optionally, you can specify with what glyph, foreground or background
        /// color to clear with. The default glyph is character 0 and the default colors will be used if
        /// none are specified.
        /// </summary>
        /// <param name="glyph">(Optional) The glyph to clear the terminal with.</param>
        /// <param name="fgColor">(Optional) The foreground color to clear the terminal with.</param>
        /// <param name="bgColor">(Optional) The background color to clear the terminal with.</param>
        public void Clear(char? glyph = null, Color? fgColor = null, Color? bgColor = null)
        {
            foreach (CharTile tile in _charTiles)
            {
                tile.Glyph = glyph ?? (char)0;
                tile.FgColor = fgColor ?? DefaultFgColor;
                tile.BgColor = bgColor ?? DefaultBgColor;
            }
        }

        /// <summary>
        /// Writes a string of multiple characters to the terminal, centered on the x position. The x position
        /// is offset by half the text length. If no foreground or background colors are specified,
        /// the default colors are used.
        /// </summary>
        /// <param name="text">The string to write to the terminal.</param>
        /// <param name="x">The x position on the terminal to write the string to.</param>
        /// <param name="y">The y position on the terminal to write the string to.</param>
        /// <param name="fgColor">(Optional) The foreground color of the string.</param>
        /// <param name="bgColor">(Optional) The background color of the string.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of the string at the position
        /// specified exceeds the terminal bounds.</exception>
        public void WriteCentered(string text, int x, int y, Color? fgColor = null, Color? bgColor = null)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            Write(text, x - (text.Length / 2), y, fgColor, bgColor);
        }

        /// <summary>
        /// Write a string of multiple characters to the terminal. If no foreground or
        /// background colors are specified, the default colors are used.
        /// </summary>
        /// <param name="text">The string to write to the terminal.</param>
        /// <param name="x">The x position on the terminal to write the string to.</param>
        /// <param name="y">The y position on the terminal to write the string to.</param>
        /// <param name="fgColor">(Optional) The foreground color of the string.</param>
        /// <param name="bgColor">(Optional) The background color of the string.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the length of the string at the position
        /// specified exceeds the terminal bounds.</exception>
        public void Write(string text, int x, int y, Color? fgColor = null, Color? bgColor = null)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            if (x <= 0 || x + text.Length >= WidthInChars - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(x), "Text must not exceed the bounds of the terminal.");
            }

            for (int i = 0; i < text.Length; i++)
            {
                Write(text[i], x + i, y, fgColor, bgColor);
            }
        }

        /// <summary>
        /// Writes a single glyph, represented by a <see cref="char"/>, to the terminal. If no foreground or
        /// background colors are specified, the default colors are used.
        /// </summary>
        /// <param name="glyph">The glyph to draw to the terminal.</param>
        /// <param name="x">The x position on the terminal to draw the glyph.</param>
        /// <param name="y">The x position on the terminal to draw the glyph.</param>
        /// <param name="fgColor">(Optional) The foreground color of the glyph.</param>
        /// <param name="bgColor">(Optional) The background color of the glyph.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the x or y position is out of
        /// the terminal bounds.</exception>
        public void Write(char glyph, int x, int y, Color? fgColor = null, Color? bgColor = null)
        {
            ValidatePosition(x, y);

            CharTile tile = _charTiles[GetIndex(x, y)];
            tile.Glyph = glyph;
            tile.FgColor = fgColor ?? DefaultFgColor;
            tile.BgColor = bgColor ?? DefaultBgColor;
        }

        /// <summary>
        /// Manually set the foreground color at a specific position.
        /// </summary>
        /// <param name="fgColor">The color to set the foreground color to.</param>
        /// <param name="x">The x position on the terminal.</param>
        /// <param name="y">The y position on the terminal.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the x or y position is out of
        /// the terminal bounds.</exception>
        public void SetForegroundColor(Color fgColor, int x, int y)
        {
            ValidatePosition(x, y);
            _charTiles[GetIndex(x, y)].FgColor = fgColor;
        }

        /// <summary>
        /// Manually set the background color at a specific position.
        /// </summary>
        /// <param name="bgColor">The color to set the background color to.</param>
        /// <param name="x">The x position on the terminal.</param>
        /// <param name="y">The y position on the terminal.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the x or y position is out of
        /// the terminal bounds.</exception>
        public void SetBackgroundColor(Color bgColor, int x, int y)
        {
            ValidatePosition(x, y);
            _charTiles[GetIndex(x, y)].BgColor = bgColor;
        }

        /// <summary>
        /// Checks whether a position is with in the terminal bounds.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the position is out of bounds.</exception>
        private void ValidatePosition(int x, int y)
        {
            if (!InBounds(x, y))
            {
                throw new ArgumentOutOfRangeException($"Position ({x}, {y}) must be between ({WidthInChars} x {HeightInChars})");
            }
        }
    }
}
