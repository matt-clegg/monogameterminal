﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameTerminal.Core
{
    /// <summary>
    /// An class used to define a character position in the terminal.
    /// </summary>
    internal class CharTile
    {
        /// <summary>
        /// The character glyph at the tile's location.
        /// </summary>
        public char Glyph { get; set; }

        /// <summary>
        /// The foreground color of the tile.
        /// </summary>
        public Color FgColor { get; set; }

        /// <summary>
        /// The background color of the tile.
        /// </summary>
        public Color BgColor { get; set; }
        
        /// <summary>
        /// Create a new instance of a character tile. The defaults you specify probably won't be rendered.
        /// </summary>
        /// <param name="glyph">Starting character glyph.</param>
        /// <param name="fgColor">Starting foreground color.</param>
        /// <param name="bgColor">Starting background color.</param>
        public CharTile(char glyph, Color fgColor, Color bgColor)
        {
            Glyph = glyph;
            FgColor = fgColor;
            BgColor = bgColor;
        }
    }
}
