﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGameTerminal.Core
{
    /// <summary>
    /// Defines a set of characters used to display glyphs on a terminal. The charset texture is only loaded
    /// when the Load method is called. If you try and draw the terminal without loading, an exception will occur.
    /// </summary>
    public class CharSet
    {
        private const int MaxGlyphs = 256;

        /// <summary>
        /// The name of the texture used by the <see cref="ContentManager"/>
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The width of an individual character.
        /// </summary>
        public int CharWidth { get; }

        /// <summary>
        /// The height of an individual character.
        /// </summary>
        public int CharHeight { get; }

        internal Texture2D Texture { get; private set; }

        /// <summary>
        /// Locations of each glyph on the texture.
        /// </summary>
        private readonly Rectangle[] _glyphBounds = new Rectangle[MaxGlyphs];

        /// <summary>
        /// Creates a new charset instance.
        /// </summary>
        /// <param name="name">The file name (excluding file extension).</param>
        /// <param name="charWidth">Character width in pixels.</param>
        /// <param name="charHeight">Character height in pixels.</param>
        /// <exception cref="ArgumentException">Thrown if no name is specified.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the char width and height is not greater than zero.</exception>
        public CharSet(string name, int charWidth, int charHeight)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Name cannot be empty.", nameof(name));
            }

            if (charWidth <= 0) throw new ArgumentOutOfRangeException(nameof(charWidth), "Character width must be greater than zero.");
            if (charHeight <= 0) throw new ArgumentOutOfRangeException(nameof(charWidth), "Character height must be greater than zero.");

            Name = name;
            CharWidth = charWidth;
            CharHeight = charHeight;
        }

        /// <summary>
        /// Called internally, loads the character set texture file. Sets the location of each glyph on the texture
        /// for lookup when rendering.
        /// </summary>
        /// <param name="content">The Monogame content manager.</param>
        internal void Load(ContentManager content)
        {
            try
            {
                Texture = content.Load<Texture2D>(Name);
            }
            catch (ContentLoadException ex)
            {
                throw new ContentLoadException($"Could not find char set texture [{Name}].", ex);
            }

            int charsWide = Texture.Width / CharWidth;
            int charsHigh = Texture.Height / CharHeight;

            for (int i = 0; i < MaxGlyphs; i++)
            {
                int x = (i % charsWide) * CharWidth;
                int y = (i / charsHigh) * CharHeight;
                _glyphBounds[i] = new Rectangle(x, y, CharWidth, CharHeight);
            }
        }

        /// <summary>
        /// Disposes any disposable assets used by the object.
        /// </summary>
        internal void Dispose()
        {
            Texture?.Dispose();
        }

        /// <summary>
        /// Gets the location of a specified glyph on the texture.
        /// </summary>
        /// <param name="glyph">The glyph represented as a char.</param>
        /// <returns>A <see cref="Rectangle"/> location of the glyph on the texture.</returns>
        /// <exception cref="IndexOutOfRangeException">Thrown if the char given is out of range.</exception>
        internal Rectangle GetBoundsForGlyph(char glyph)
        {
            if (glyph >= 0 && glyph < MaxGlyphs)
            {
                return _glyphBounds[glyph];
            }

            throw new IndexOutOfRangeException($"[{glyph}] is not a valid glyph.");
        }

    }
}
